# Revision 3 (2.2) - Power Module - OSV Velocity team - DCDC

## Designer

Federico De Benedetti (OSV Velocity Team - INFN Milan Division), Main design
Nadim Conti (OSV Velocity Team - INFN Milan Division), System integration

## Main specifications

- Input voltage 48V 4.5A

- Output voltage:
    - 12v   1000mA
    - 5v    500mA
    - 3v3   500mA
    
- Nominal overcurrent Fuse protection:
    - 6.3 A (FAST)
    
## Changelog over Rev2 DCDC
- moved overvoltage protections
- added 2x 22 pos connectors instead of 11 pos ones to improve mechanical stability, thermal profile and enable more signals to be carried out to the motherboard
    
## Note:

The usage of this circuit at high input voltages and high 5 or 3v3 currents is discouraged. 
Disregarding the previous point may lead to permanent failure of the DCDC converters.
